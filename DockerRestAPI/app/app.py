import os
import flask
import flask_wtf
from collections import OrderedDict
import json 
from flask_restful import Resource, Api
from wtforms import (StringField, PasswordField, SubmitField, BooleanField)
from wtforms.validators import DataRequired
from flask import (Flask, redirect, url_for, request, render_template, 
                    jsonify, abort, session, Response)
from flask_login import (LoginManager, current_user, login_required,
                            login_user, logout_user, UserMixin, 
                            confirm_login, fresh_login_required)
from pymongo import MongoClient
import acp_times
import hashlib
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import time
from functools import wraps

app = Flask(__name__)
api = Api(app)
app.config["SECRET_KEY"] = "a"
client = MongoClient('mongodb:27017')
db = client.users
times = client.times
login_manager = LoginManager()
login_manager.init_app(app)


class User(UserMixin):
    def __init__(self, name, id, password, active=True):
        self.name = name
        self.id = id 
        self.active = active
        db.users.insert_one({"id": id, "name": name, "password": password})
    
    def is_active(self):
        return True 

    def is_authenticated(self):
        return True

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in = expiration)
        return s.dumps({ 'id': self.id })

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None # valid token, but expired
        except BadSignature:
            return None # invalid token
        user = load_user(data['id'])
        return user


@login_manager.user_loader
def load_user(id):
    user = db.users.find_one({"id": int(id)})
    print(id)
    print("a user just got loaded")
    if not user:
        print("we don't got one!")
        return None
    return User(user["name"], user["id"], user["password"])

login_manager.needs_refresh_message = (
    u"To protect your account, please reauthenticate to access this page."
)

login_manager.needs_refresh_message_category = "info"
login_manager.login_view = "login"

class RegisterForm(flask_wtf.FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember = BooleanField('Remember Me')
    login = SubmitField('Login')

class LoginForm(flask_wtf.FlaskForm):
    username = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        name = form.username.data
        password = form.password.data
        user = db.users.find_one({"name": name})
        if user:
            if pwd_context.verify(password, user["password"]):
                user = load_user(user["id"])
                global current_user
                current_user = user
                print(current_user.name)
                return flask.redirect(url_for("index"))
    return flask.render_template('login.html', form=form)


@app.route("/register", methods=["GET", "POST"])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
     #   return url_for("api_register", username=form.username.data, 
      #                  password=form.password.data, remember=form.remember.data)
      pass
    return render_template("register.html", form=form)

@app.route('/api/register/', methods=["POST"])
def api_register():
   # name = request.json.get('username')
    #password = request.json.get('password')
    #remember = request.json.get('remember')
    form = RegisterForm()
    name = form.username.data
    password = form.password.data
    remember = form.remember.data
    if not name or not password:
        abort(400)
    existing_user = db.users.find_one({"name": name})
    if existing_user:
        if password == existing_user["password"]:
            newuser = existing_user
            exists = True
        else:
            flask.flash(u"This user already exists")
            abort(400)
    else:
        id = int(hashlib.sha1(name.encode('utf-8')).hexdigest(), 16) % (10 ** 8)
        hashed_pass = pwd_context.encrypt(password)
        newuser = User(name, id, hashed_pass)
        login_user(newuser, remember=remember)
    return jsonify({'username': newuser.name}), 201, {'Location': url_for('get_user', id=newuser.id)} # TODO


@app.route('/api/token', methods=["GET", "POST"])
def get_token():
    form = LoginForm()
    if form.validate_on_submit():
        name = form.username.data
        password = form.password.data
        user = db.users.find_one({"name": name})
        if user:
            if pwd_context.verify(password, user["password"]):
                user = load_user(user["id"])
                print(login_user(user))
                flask.flash("success")
                print("success")
                return jsonify({'token': user.generate_auth_token(user.id).decode('utf-8'), 'duration': 600})
        else:
            print("no recpdr")
            flask.flash("We do not appear to have a record of this username")
            abort(401)
        abort(401)
    return flask.render_template("login.html", form=form)


@app.route('/') 
def index():
    return render_template('calc.html')


@app.route('/api/users/<int:id>')
def get_user(id):
    user = current_user 
    if not user:
        abort(400)
    return jsonify({'username': user.username})


@app.route('/_submit_times')
def print_controls():
    """"
    When sent a JSON object from calc.html specifying brevets and controls,
    this function stores this information in a the database, along with the
    open and close times for each brevet.
    """
    db.times.drop()
    brevet = request.args.get('brevet', type=int)
    datetime = request.args.get('datetime', type=str)
    controls = request.args.get('controls', type=str)
    print(controls)
    controls = eval(controls) # Not safe!
    controls = sorted([int(round(float(x))) for x in controls])
    if not controls:
        return flask.jsonify(result="No control error")
    if any([x for x in controls if x > (1.2 * brevet)]):
        return flask.jsonify(result="Control too big error")
    if any([x for x in controls if x < 0]):
        return flask.jsonify(result="Control below zero error")

    db.brevet.insert_one({'name': 'brevet', 'description': 'brevet',
                         'kms': brevet})
    for control in controls:
        name = f'{str(control) + "kms"}'
        open_time = acp_times.open_time(control, brevet, datetime)
        close_time = acp_times.close_time(control, brevet, datetime)
        db.times.insert_one({'name': name, 'description': 'times',
                             'open': open_time, 'close': close_time})
        for item in db.times.find():
            print(item['name'])
            if 'control' in item['name']:
                print(item['close'])
            elif item['name'] == 'brevet':
                print(item['kms'])
    return flask.jsonify(result="Success")


"""
I did not realize I would still need to populate the open and close fields in
calc.html, hence the mess this turned into. I'll refactor this if I find the
time.
"""


@app.route('/_calc_times')
def _calc_times():
    
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet = request.args.get('brevet', type=int)
    datetime = request.args.get('datetime', type=str)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args)) 
    open_time = acp_times.open_time(km, brevet, datetime)
    close_time = acp_times.close_time(km, brevet, datetime)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/_view_times')
def view():
    _items = db.times.find()
    items = [item for item in _items]
    return render_template('todo.html', items=items)


#@app.route('/<path: path>'):
#   cwd = os.getcwd()
#    if not os.path.isfile(cwd + '/templates/' + path):
#        return '<html> <head> 404 </head> </html>', 404
#    return render_template(path)

@app.route('/invalid')
def invalid():
    return render_template('invalid.html')


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))


class ViewAllTimes(Resource):
    def get(self):
        top = request.args.get('top')
        token = request.args.get('token')
        if token == None:
            abort(401)
        user = User.verify_auth_token(token)
        if user is None:
            abort(401)
        if top != None:  # I check twice. Not great.
            top = int(top)
        _items = db.times.find()
        items = [item for item in _items]
        items = sorted(items, key=lambda item: item['name'])
        if top == None or top > len(items):
            top = len(items)
        times = [f'open: {i["open"]}, close: {i["close"]}' for i in items]
        controls = [item['name'] for item in items]
        control_times = OrderedDict()
        for i, control in enumerate(controls[-top:]):
            control_times[control] = times[i]
        return flask.jsonify(control_times)


api.add_resource(ViewAllTimes, '/listAll/<string:format>', '/listAll')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
