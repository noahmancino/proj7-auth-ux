name: noah mancino
email: noahdanielmancino@gmail.com

The main page at "/" has links to /register, /api/token, and an endpoint that logs you out. /api/token requires a login regardless of whether you were logged in before (I should have asked if this is right, I'm not sure). The endpoint for my resource is /listAll. In order to accsess it, you have to append "token=whatever-authentication-token-you-got" into the url as a query string. The only purpose of logging in is displaying a message on the home page, because everything else either requires a fresh login or a token. Sorry that I didn't maintain this project better, I'm sure it will be a mess to grade. 

Another thing I noticed just before turning this in: the part of the homepage that converts miles got broken somewhere along the way. Data must be enter into the km colums only.
